package de.hcinc.torasu.std.rnd;

import java.util.Map;

import de.hcinc.torasu.core.DataPackageable;
import de.hcinc.torasu.core.Dependence;
import de.hcinc.torasu.core.Dependence.DependenceStack;
import de.hcinc.torasu.core.Element;
import de.hcinc.torasu.core.Element.Rewritable;
import de.hcinc.torasu.core.ExecutionRequirements;
import de.hcinc.torasu.core.PipelineSettings;
import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.core.RenderContextMask;
import de.hcinc.torasu.core.Renderable;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultFormatSettings;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultSegmentSettings;
import de.hcinc.torasu.log.LogEntry.LogLevel;
import de.hcinc.torasu.log.LogEntry.LogMessage;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.pipeline.SystemResourceInterface;
import de.hcinc.torasu.util.ElementRenderContextMask;
import de.hcinc.torasu.util.ReadySessionManager;
import de.hcinc.torasu.util.RenderUtils;

public class RMultiply implements Renderable, Rewritable {

	private ReadySessionManager rsm = new ReadySessionManager();
	
	private Renderable rx;
	private Renderable ry;

	
	public RMultiply(Renderable x, Renderable y) {
		this.rx = x;
		this.ry = y;
	}
	
	/*
	*	RENDERABLE
	*/
	
	@Override
	public RenderResult render(RenderWrangler rw) {
		ResultSegmentSettings rseg = RenderUtils.findRSEGbyIdent(rw.getResultSettings(), "de.hcinc.torasu.numeric");
		
		if (rseg != null) {

			ExecutionInterface ei = rw.getExecutionInterface();
			RenderContext rctx = rw.getRenderContext();

			long rxid = ei.enqueueRender(rx, 
					new RenderContext(rctx.getValueObjects(), null),
					RenderUtils.simpleSettings("v", "de.hcinc.torasu.numeric", 
						new ResultFormatSettings("java.primitive.double", null, null)), 
					0);
			
			long ryid = ei.enqueueRender(ry, 
					new RenderContext(rctx.getValueObjects(), null),
					RenderUtils.simpleSettings("v", "de.hcinc.torasu.numeric", 
						new ResultFormatSettings("java.primitive.double", null, null)), 
					0);
			
			RenderResult rxr = ei.fetchRenderResult(rxid);
			RenderResult ryr = ei.fetchRenderResult(ryid);
			
			
			if (rxr.getStatus()<0 || ryr.getStatus()<0) {
				return new RenderResult(-1, null); //-1: Undefined Error
			} else {
				if (rxr.getStatus() != 4 && ryr.getStatus() != 4) {//Check if a result got applied
					
					double result = ((double)rxr.getResult()[0].getResult())*((double)ryr.getResult()[0].getResult());
					
					rw.getLogInstruction().log(new LogMessage(LogLevel.Debug, "MUL RESULT: " + result));
					
					return RenderUtils.simpleResult(0, rseg.getKey(), "java.primitive.double", result); //0: OK and result applied
					
				} else {

					return RenderUtils.simpleResult(6, rseg.getKey(), "java.primitive.double", 0D); //6: Result applied, but crucial data missing
					
				}
			}

		} else {
			return new RenderResult(4, null); //4: No result applied
		}
		
		
	}

	//No RenderableData provided here
	@Override
	public RenderableData data(String[] keys, RenderContext rctx) {return null;}
	
	//No Modifications
	@Override
	public String[] promiseRenderModification(String[] requests, RenderContext rctx) {return null;}

	//Single-threaded
	@Override
	public ExecutionRequirements getExecutionRequirements(RenderContext rctx, String[] segments) {
		return new ExecutionRequirements(1);
	}
	
	//Mask: Mask the required values by rx and ry
	@Override
	public RenderContextMask getRenderContextMask(String[] segments) {
		return new ElementRenderContextMask(
				new ElementRenderContextMask.MaskInclude(rx, "de.hcinc.torasu.numeric"),
				new ElementRenderContextMask.MaskInclude(ry, "de.hcinc.torasu.numeric")
			);
	}
	
	//No specific PipelineSettings
	@Override
	public PipelineSettings getPipelineSettings() {return null;}

	/*
	*	ELEMENT
	*/

	//Name: Multiply
	@Override
	public String getName() {
		return "Multiply";
	}

	//No extra information
	@Override
	public String[] getInfo() {
		return null;
	}
	
	//Initialize a ready-session
	public long readySession(RenderContext rctx, String[] operations) {
		return rsm.add(rctx, operations).getReadyId(); //add to rsm
	}
	
	//Update a ready-session
	public void readySessionUpdate(long readyId, RenderContext rctx, boolean updateValues) {
		rsm.get(readyId).update(rctx, updateValues); //update in rsm
	}
	
	//Nothing to ready here
	@Override
	public ReadyRefreshPolicy readySelf(long readyId, int pld, SystemResourceInterface sysri, ExecutionInterface ei) {
		return new ReadyRefreshPolicy(Double.NaN, false, false);
	}

	//Remove ready-session
	@Override
	public void unready(long readyId) {
		rsm.remove(readyId); //remove from rsm
	}
	
	//The object itself is always ready
	@Override
	public boolean isReady(RenderContext rctx, String[] operations) {
		return true; //ready
	}

	@Override
	public DependenceStack dependencies(long readyId, double weightIndex, int index, ExecutionInterface ei) {
		if (weightIndex >= 1) {
			RenderContext rctx = rsm.get(readyId).getRenderContext();
			RenderContext nrctx = new RenderContext(rctx.getValueObjects(), null);
			return DependenceStack.makeSimpleStack(
					new ReadyRefreshPolicy(Double.NaN, true, false),
					new Dependence(rx, nrctx, new String[] {"de.hcinc.torasu.numeric"}), 
					new Dependence(ry, nrctx, new String[] {"de.hcinc.torasu.numeric"}));
		} else {
			return null;
		}
	}

	/*
	*	REWRITABLE
	*/

	@Override
	public void rewrite(DataPackageable data, Map<String, Element> elements) {
		Element ex = elements.get("x");
		Element ey = elements.get("y");
		
		if (ex == null) {throw new IllegalArgumentException("element \"x\" is null, Renderable expected");}
		if (ey == null) {throw new IllegalArgumentException("element \"y\" is null, Renderable expected");}
		if (!(ex instanceof Renderable)) {throw new IllegalArgumentException("element \"x\" is no Renderable, but a Renderable was expected");}
		if (!(ey instanceof Renderable)) {throw new IllegalArgumentException("element \"y\" is no Renderable, but a Renderable was expected");}

		rx = (Renderable) ex;
		ry = (Renderable) ey;
	}

	@Override
	public int overrideElement(String key, Element elem) {
		switch (key) {
		case "x":
			if (elem instanceof Renderable) {
				Renderable rnd = (Renderable) elem;
				rx = rnd;
				return 0; //OK
			} else {
				return -32; //Illegal Arguments, invalid value (Here: no Renderable supplied)
			}
			
		case "y":
			if (elem instanceof Renderable) {
				Renderable rnd = (Renderable) elem;
				rx = rnd;
				return 0; //OK
			} else {
				return -32; //Illegal Arguments, invalid value (Here: no Renderable supplied)
			}

		default:
			return -31; //Illegal Arguments, invalid key
		}
	}
	
	/**
	 * 	Element-Clone
	 */
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
	
}
