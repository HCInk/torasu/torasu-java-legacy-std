package de.hcinc.torasu.std.rnd;

import de.hcinc.torasu.core.Dependence.DependenceStack;
import de.hcinc.torasu.core.Element.Rewritable;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultFormatSettings;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultSegmentSettings;
import de.hcinc.torasu.log.LogEntry.LogLevel;
import de.hcinc.torasu.log.LogEntry.LogMessage;

import java.util.HashMap;
import java.util.Map;

import de.hcinc.torasu.core.DataPackageable;
import de.hcinc.torasu.core.Dependence;
import de.hcinc.torasu.core.Element;
import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.core.RenderContextMask;
import de.hcinc.torasu.core.Renderable;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.pipeline.SystemResourceInterface;
import de.hcinc.torasu.util.ElementRenderContextMask;
import de.hcinc.torasu.util.RenderUtils;
import de.hcinc.torasu.util.RewriteHelper;
import de.hcinc.torasu.util.SimpleRenderable;
import de.hcinc.torasu.util.RewriteHelper.ElementPointer;

public class RMultiplySimple extends SimpleRenderable implements Rewritable {
	
	private ElementPointer<Renderable> rx;
	private ElementPointer<Renderable> ry;
	
	RewriteHelper rwh;
	private RenderContextMask rctxm;

	
	public RMultiplySimple(Renderable x, Renderable y) {
		this.rx = new ElementPointer<Renderable>(Renderable.class, x);
		this.ry = new ElementPointer<Renderable>(Renderable.class, y);
		
		HashMap<String, ElementPointer<?>> map = new HashMap<String, RewriteHelper.ElementPointer<?>>();
		map.put("x", rx);
		map.put("y", ry);
		rwh = new RewriteHelper(map);
		
		buildRCTXM();
	}
	
	private void buildRCTXM() {
		this.rctxm = new ElementRenderContextMask(
			new ElementRenderContextMask.MaskInclude(rx.getValue(), "de.hcinc.torasu.numeric"),
			new ElementRenderContextMask.MaskInclude(ry.getValue(), "de.hcinc.torasu.numeric")
		);
	}
	
	/*
	*	Render
	*/
	
	@Override
	public RenderResult render(RenderWrangler rw) {
		ResultSegmentSettings rseg = RenderUtils.findRSEGbyIdent(rw.getResultSettings(), "de.hcinc.torasu.numeric");
		
		if (rseg != null) {

			ExecutionInterface ei = rw.getExecutionInterface();
			RenderContext rctx = rw.getRenderContext();

			long rxid = ei.enqueueRender(rx.getValue(), 
					new RenderContext(rctx.getValueObjects(), null),
					RenderUtils.simpleSettings("v", "de.hcinc.torasu.numeric", 
						new ResultFormatSettings("java.primitive.double", null, null)), 
					0);
			
			long ryid = ei.enqueueRender(ry.getValue(), 
					new RenderContext(rctx.getValueObjects(), null),
					RenderUtils.simpleSettings("v", "de.hcinc.torasu.numeric", 
						new ResultFormatSettings("java.primitive.double", null, null)), 
					0);
			
			RenderResult rxr = ei.fetchRenderResult(rxid);
			RenderResult ryr = ei.fetchRenderResult(ryid);
			
			
			if (rxr.getStatus()<0 || ryr.getStatus()<0) {
				return new RenderResult(-1, null); //-1: Undefined Error
			} else {
				if (rxr.getStatus() != 4 && ryr.getStatus() != 4) {//Check if a result got applied
					
					double result = ((double)rxr.getResult()[0].getResult())*((double)ryr.getResult()[0].getResult());
					
					rw.getLogInstruction().log(new LogMessage(LogLevel.Debug, "MUL RESULT: " + result));
					
					return RenderUtils.simpleResult(0, rseg.getKey(), "java.primitive.double", result); //0: OK and result applied
					
				} else {

					return RenderUtils.simpleResult(6, rseg.getKey(), "java.primitive.double", 0D); //6: Result applied, but crucial data missing
					
				}
			}

		} else {
			return new RenderResult(4, null); //4: No result applied
		}
		
	}

	@Override
	public String getName() {
		return "Multiply";
	}

	@Override
	public RenderContextMask getRenderContextMask(String[] segments) {
		return rctxm;
	}

	@Override
	public ReadyRefreshPolicy readySelf(long readyId, int pld, SystemResourceInterface sysri, ExecutionInterface ei) {
		return ReadyRefreshPolicy.NEVER;
	}

	@Override
	public boolean isReady(RenderContext rctx, String[] operations) {
		return true;
	}

	@Override
	public void cleanReady(long readyId) {}

	@Override
	public DependenceStack dependencies(long readyId, double weightIndex, int index, ExecutionInterface ei) {
		if (weightIndex >= 1) {
			RenderContext rctx = rsm.get(readyId).getRenderContext();
			RenderContext nrctx = new RenderContext(rctx.getValueObjects(), null);
			return DependenceStack.makeSimpleStack(
					new ReadyRefreshPolicy(Double.NaN, true, false),
					new Dependence(rx.getValue(), nrctx, new String[] {"de.hcinc.torasu.numeric"}), 
					new Dependence(ry.getValue(), nrctx, new String[] {"de.hcinc.torasu.numeric"}));
		} else {
			return null;
		}
	}
	
	/*
	*	REWRITABLE
	*/

	@Override
	public void rewrite(DataPackageable data, Map<String, Element> elements) {
		rwh.overrideAll(elements);
		buildRCTXM();
	}

	@Override
	public int overrideElement(String key, Element elem) {
		rwh.overrideElement(key, elem);
		buildRCTXM();
		return 0;
	}

	/*
	 * 		CLONABLE
	 */
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// FIXME Support clone
		throw new CloneNotSupportedException();
	}
	
}
