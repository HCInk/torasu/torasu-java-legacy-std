package de.hcinc.torasu.std.rnd;

import java.util.Map;

import de.hcinc.torasu.core.DataPackageable;
import de.hcinc.torasu.core.Dependence.DependenceStack;
import de.hcinc.torasu.core.Element;
import de.hcinc.torasu.core.Element.Rewritable;
import de.hcinc.torasu.core.ExecutionRequirements;
import de.hcinc.torasu.core.PipelineSettings;
import de.hcinc.torasu.core.RenderContext;
import de.hcinc.torasu.core.RenderContextMask;
import de.hcinc.torasu.core.Renderable;
import de.hcinc.torasu.core.Renderable.ResultSettings.ResultSegmentSettings;
import de.hcinc.torasu.pipeline.ExecutionInterface;
import de.hcinc.torasu.pipeline.SystemResourceInterface;
import de.hcinc.torasu.std.dp.DDoubleValue;
import de.hcinc.torasu.util.RenderUtils;

public class RFixedDouble implements Renderable, Rewritable {
	
	private double val;

	public RFixedDouble(double val) {
		this.val = val;
	}
	
	/*
	*	RENDERABLE
	*/
	
	//Render the double-value
	@Override
	public RenderResult render(RenderWrangler rw) {
		ResultSegmentSettings rseg = RenderUtils.findRSEGbyIdent(rw.getResultSettings(), "de.hcinc.torasu.numeric");
		
		if (rseg != null) {
		
			return RenderUtils.simpleResult(0, rseg.getKey(), "java.primitive.double", val); // 0: OK and result applied
		
		} else {
			return new RenderResult(4, null); //4: No result applied
		}
		
	}

	//No RenderableData provided here
	@Override
	public RenderableData data(String[] keys, RenderContext rctx) {return null;}
	
	//No Modifications
	@Override
	public String[] promiseRenderModification(String[] requests, RenderContext rctx) {return null;}

	//Single-threaded
	@Override
	public ExecutionRequirements getExecutionRequirements(RenderContext rctx, String[] segments) {
		return new ExecutionRequirements(1);
	}
	
	//Mask: Nothing from the RenderContext required
	@Override
	public RenderContextMask getRenderContextMask(String[] segments) {
		return RenderContextMask.NONE;
	}
	
	//No specific PipelineSettings
	@Override
	public PipelineSettings getPipelineSettings() {return null;}

	/*
	*	ELEMENT
	*/
	
	//Name with contained value
	@Override
	public String getName() {
		return "FixedValue(" + val + ")";
	}

	//No extra information
	@Override
	public String[] getInfo() {
		return null;
	}

	//No ready session available
	public long readySession(RenderContext rctx, String[] operations) {
		return -2; //Nothing to make ready
	}

	//Do nothing, since no ready session available
	public void readySessionUpdate(long readyId, RenderContext rctx, boolean updateValues) {}
	
	//No function on making ready, always ready
	@Override
	public ReadyRefreshPolicy readySelf(long readyId, int pld, SystemResourceInterface sysri, ExecutionInterface ei) {return null;}

	//No function on removing readiness, always ready
	@Override
	public void unready(long readyId) {}

	//Always ready
	@Override
	public boolean isReady(RenderContext rctx, String[] operations) {
		return true; //Ready
	}
	
	//No Dependencies
	@Override
	public DependenceStack dependencies(long readyId, double weightIndex, int index, ExecutionInterface ei) {
		return null; //No dependencies
	}

	/*
	*	REWRITABLE
	*/
	
	//Rewrite double-value
	@Override
	public void rewrite(DataPackageable data, Map<String, Element> elements) {
		if (data instanceof DDoubleValue) {
			DDoubleValue dv = (DDoubleValue) data;
			val = dv.getValue();
		} else {
			throw new IllegalArgumentException("Expected different DataPackageable: Got " + data.getClass() + " (Ident: " + data.getIdent() + ") but expected " + DDoubleValue.class + " (Ident: de.hcinc.std.dp.double)");
		}
	}

	//No contained elements to override
	@Override
	public int overrideElement(String key, Element elem) {
		return -3; //Illegal Arguments
	}
	
	/**
	 * 	Element-Clone
	 */
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

}
