package de.hcinc.torasu.std.dp;

import org.json.JSONObject;

import de.hcinc.torasu.core.DataPackageable;

public class DDoubleValue implements DataPackageable {

	private double val;

	public DDoubleValue(double val) {
		this.val = val;
	}
	
	public double getValue() {
		return val;
	}

	
	@Override
	public String getIdent() {
		return "de.hcinc.std.dp.double";
	}
	
	@Override
	public String[][] getData() {
		return new String[][] {
			new String[] {
				"v", Double.toString(val)
			}
		};
	}
	

	public static class DDoubleValueImporter implements DataPackageableImporter {

		@Override
		public String getIdent() {
			return "de.hcinc.std.dp.double";
		}

		@Override
		public DataPackageable importObject(JSONObject json) {
			return new DDoubleValue(json.getDouble("v"));
		}
		
	}


}
